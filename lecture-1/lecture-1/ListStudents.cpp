//
//  ListStudents.cpp
//  lecture-1
//
//  Created by Преподаватель on 25.03.2021.
//  Copyright © 2021 Преподаватель. All rights reserved.
//

#ifndef LISTSTUDENTS_H
#define LISTSTUDENTS_H

#include <stdio.h>
#include "Student.cpp"
#include <iostream>
#include <iomanip>
using namespace std;

// Класс обертка, содержащий список студентов
class Students{
public:
    // Конструктор для инициализации списка
    Students(int countStudents = 10){
        this->fullStudents = countStudents;
        students = new Student[countStudents];
        // cout << "Конструктор список " << this << endl;
    }
    // Конструктор копирования
    Students(const Students &stud){
        // Добавить реализацию
    }
    
    // Метод добавления данных о студенте в список
    void add(int id, const char* name, float avg){
        if (currentIndex < fullStudents){
                students[currentIndex] = Student(id, name, avg);
            currentIndex++;
            checkFullMass(currentIndex);
        }
        else
            cout << "Слишком много студентов" << endl;
    }
    // Метод добавления экземпляра Студент в список
    void add(Student student){
        if (currentIndex < fullStudents){
                students[currentIndex] = student;
            currentIndex++;
            checkFullMass(currentIndex);
        }
        else
            cout << "Слишком много студентов" << endl;
    }
    
    // Метод возвращающий количество студентов в списке
    int counts(){
        return this->currentIndex;
    }
    
    // Метод удаления студента из списка
    void remove(int atStudent){
        for (int i = atStudent; i < currentIndex - 1; i++) {
            students[i] = students[i + 1];
        }
        currentIndex--;
    }
    
    // Метод вывода списка в консоль
    void display(){
        for (int i = 0; i < currentIndex; i++) {
            students[i].showStudent();
        }
        cout << "--------------------------------" << endl;
    }
    
    // Перегрузка оператора = для выполнения корректного присваивания
    Students& operator =(Students& studs){
        this->currentIndex = studs.currentIndex;
        this->fullStudents = studs.fullStudents;
        students = new Student[this->fullStudents];
        for (int i = 0; i < currentIndex; i++) {
            students[i] = studs.students[i];
        }
        return *this;
    }
    
    // Перегрузка оператора [] для элемента по индексу
    Student& operator [](int index){
        return students[index];
    }
    
    // Перегрузка оператора << для вывода списка оператором cout
    friend ostream& operator <<(ostream &os, Students& stud){
        for (int i = 0; i < stud.currentIndex; i++) {
            cout << stud.students[i];
        }
        cout << "--------------------------------" << endl;
        return os;
    }
    
    // Метод поиска всех студентов по заданному критерию
    // Критерий поиска формируется внешней функцией (делегат)
    Students* findAll(const function<bool(Student&)>& predicate){
        // Создание нового списка студентов для поиска
        static Students findStudents(currentIndex);
        for (int i = 0; i < currentIndex; i++)
            if (predicate(students[i]))
                findStudents.add(students[i]);
        return findStudents.counts() > 0 ? &findStudents : NULL;
    }
    
    // Метод поиска первого студента, который удовлетворяет
    // заданному критерию
    // Критерий поиска формируется внешней функцией (делегат)
    Student* find(const function<bool(Student&)>& predicate){
        for (int i = 0; i < currentIndex; i++)
            if (predicate(students[i]))
                return &students[i];
        return NULL;
    }
    
    // Метод сортировки "Пузырьком"
    Students& sort(const function<bool(Student&, Student&)>& predicate){
        // Создание нового списка студентов для сортировки
        static Students sortStudents;
        sortStudents = *this;
        for (int i = 0; i < currentIndex; i++)
            for (int j = i + 1; j < currentIndex; j++)
                if (predicate(sortStudents[i], sortStudents[j]))
                    sortStudents.Swap(i, j);
        return sortStudents;
    }
    
    // Метод, который выполняет обменную операцию элементов
    // Параметры: index1 - первый эл., index2 - второй эл.
    void Swap(int index1, int index2){
        Student tempStudent;
        tempStudent = this->students[index1];
        this->students[index1] = this->students[index2];
        this->students[index2] = tempStudent;
    }
    
    ~Students(){
        delete [] students;
    }
private:
    Student *students;      // Список студентов
    int currentIndex = 0;   // Текущий индекс
    int fullStudents = 0;   // Заданное количество записей
    
    void checkFullMass(int count){
        if (currentIndex == fullStudents){
            cout << "Список студентов заполнен!" << endl;
        }
    }
};

#endif
