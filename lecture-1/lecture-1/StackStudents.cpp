//
//  StackStudents.cpp
//  lecture-1
//
//  Created by Преподаватель on 25.03.2021.
//  Copyright © 2021 Преподаватель. All rights reserved.
//
#ifndef STACKSTUDENTS_H
#define STACKSTUDENTS_H

#include <stdio.h>
#include <cassert>
#include <iostream>
#include "Student.cpp"

using namespace std;

class StackStudents{

public:
    // Конструктор для инициализации стека
    // По умолчанию размер стека равен 10 элементам
    StackStudents(int countStudents = 10){
        this->fullStudents = countStudents;
        stack = new Student[countStudents];
    }
    
    // Конструктор копирования
    StackStudents(const StackStudents &otherStack){
        // Добавить реализацию
    }
    
    // Метод добавления данных о студенте в стек
    void push(int id, const char* name, float avg){
        if (top < fullStudents - 1){
                stack[++top] = Student(id, name, avg);
        }
        else
            cout << "Стек заполнен" << endl;
    }
    // Метод добавления экземпляра Студент в стек
    void push(const Student &student){
        if (top < fullStudents - 1){
                stack[++top] = student;
        }
        else
            cout << "Стек заполнен" << endl;
    }
    
    // Метод вызврата студента из вершины стека и удаления его
    Student* pop(){
        if (top != -1)
            return &stack[top--];
        return NULL;
    }
    
    // Метод вызврата студента из вершины стека и без удаления
    const Student& peek(){
        return stack[top];
    }
    
    // Метод вызврата студента по индексу от вершины стека
    const Student* peek(int index){
        if(index >= 0 && index <= top)
            return &stack[top - index];
        return NULL;
    }
    
    // Метод вывода стека в консоль
    void display(){
        if (top > -1) {
            for (int i = top; i >= 0; i--) {
                cout << stack[i];
            }
            cout << "--------------------------------" << endl;
        }
        else
            cout << "[]" << endl;
    }
    
    // Метод возвращающий количество студентов в стеке
    int counts(){
        return this->top;
    }
    
    // Метод возвращающий размерность стека студентов
    int size(){
        return this->fullStudents;
    }
    
    // Перегрузка оператора = для выполнения корректного присваивания
    
    // Перегрузка оператора << для вывода списка оператором cout
    
    // Перегрузка оператора +
    
    // Перегрузка оператора +=
    
    // Перегрузка оператора ==
    
    // Деструктор стека
    ~StackStudents(){
        delete [] stack;
    }
    
private:
    Student* stack;     // Стек студентов
    int fullStudents;   // Заданное количество записей
    int top = -1;       // Индекс вершины стека
};

#endif
