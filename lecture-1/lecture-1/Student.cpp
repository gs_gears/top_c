//
//  Student.cpp
//  lecture-1
//
//  Created by Преподаватель on 25.03.2021.
//  Copyright © 2021 Преподаватель. All rights reserved.
//
#ifndef STUDENT_H
#define STUDENT_H

#include <stdio.h>
#include <iostream>
#include <iomanip>
using namespace std;

class Student{
public:
    // Именнованные свойства
    void setId(int value){
        this->id = value > 100 || value < 1 ? -1 : value;
    }
    int getId(){
        return this->id;
    }
    void setName(const char* value){
        this->fullName = value;
    }
    const char* getName(){
        return this->fullName;
    }
    void setAvg(float value){
        this->avg = value > 5 || value < 2 ? -1 : value;
    }
    float getAvg(){
        return this->avg;
    }
    // Конструкторы
    Student(){
        
    }
    Student(int id, const char* name, float avg){
        setId(id);
        setName(name);
        setAvg(avg);
    }
    // Методы
    void inputStudent(){
        cout << "Введите номер зачетки ";
        int id; cin >> id; setId(id);
        cout << "Введите ФИО студента ";
        cin.ignore();
        char* name = new char[15];
        cin.getline(name, 15); setName(name);
        cout << "Введите средний балл ";
        int avg; cin >> avg; setAvg(avg);
        delete [] name;
    }
    ~Student(){

    }
    
    bool operator ==(Student& stud){
        return this->id == stud.id &&
        strcmp(this->fullName, stud.fullName) == 0 &&
        this->avg == stud.avg;
    }
    
    void showStudent(){
        cout << "Номер зачетки " << id << endl;
        cout << "ФИО студента " << fullName << endl;
        cout << "Средний балл " << avg << endl;
    }

    friend ostream& operator<<(ostream &os, Student &stud){
        cout << "Номер зачетки " << stud.id << " | ФИО: " << stud.fullName << "\t\t" << " | ср. балл " << stud.avg << endl;;
        return os;
    }
private:
    int id;                     // Номер зачетной книжки
    const char *fullName;       // Полное ФИО студента
    float avg;                  // Средний балл за сессию
};

#endif
