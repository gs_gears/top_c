//
//  main.cpp
//  lecture-3
//  Перегрузка операторов
//  Created by Преподаватель on 25.02.2021.
//  Copyright © 2021 Преподаватель. All rights reserved.
//

#include "Student.cpp"
#include "ListStudents.cpp"
#include "StackStudents.cpp"

// Пердикаты поиска студентов по критерию
bool findAvg(Student& stud){
    return stud.getAvg() > 3;
}
bool findId(Student& stud){
    return stud.getId() == 3;
}

int main(int argc, const char * argv[]) {
    
//    Students students(5);
//    students.add(Student(5, "Иванов Иван", 5));
//    students.add(Student(2, "Петров Петр", 4));
//    students.add(Student(3, "Макаров Сергей", 4));
//    students.add(4, "Владимиров Петр", 4);
//    students.add(1, "Сидоров Петр", 3);
//    cout << students[2];
//    students[10].showStudent();
//    students.display();
//    students.remove(2);
//    students.display();
//    cout << students;


    
//    Students findStud;
//    Students* s = students.findAll([](Student& stud){
//        return stud.getId() > 3;
//    });
//    if (s != NULL) {
//        findStud = *s;
//    }
//    cout << students;
//    Students sortStud;
//    sortStud = students.sort([](Student& stud1, Student& stud2){
//        return stud1.getId() < stud2.getId();
//    });
//    cout << sortStud;
    
    StackStudents students(4);
//    students.push(Student(1, "Иванов Иван", 5));
//    students.push(Student(2, "Петров Петр", 4));
//    students.push(Student(3, "Макаров Сергей", 4));
//    students.push(4, "Владимиров Петр", 4);
//    students.push(5, "Сидоров Петр", 3);
    
//    for (int i = 0; i<5; i++) {
//        cout << *(students.pop());
//    }
    
//    students.peek(9);
    students.display();
    
    
}
